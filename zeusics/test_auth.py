import requests

from .data import *

def test_zeus_auth(auth_token) -> bool:
    url = f"{ZEUS_URL}/api/User/testAuth"

    print("Testing Zeus auth ...")

    headers_dict = {"Authorization": f"Bearer {auth_token}"}
    response = requests.get(url, headers=headers_dict)
    return response.status_code == 200
