import time
import json

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

from .local_storage import LocalStorage
from zeusics.data import *

def login_o365(browser, email, password) -> bool:
    time.sleep(2)
    elem = browser.find_elements_by_class_name(ZEUS_LOGIN_O365_BTN_CLASS)
    if not elem:
        return False

    print("Trying to log with Office 365 ...")
    elem[0].click()
    time.sleep(3)

    return o365_fill_form(browser, email, password)


def o365_stay_signed(browser, enabled=False) -> bool:
    try:
        yes_button = browser.find_element_by_id(ZEUS_O365_STAY_YES)
        no_button = browser.find_element_by_id(ZEUS_O365_STAY_NO)

        if enabled:
            yes_button.click()
        else:
            no_button.click()
    except Exception:
        pass

    time.sleep(2)
    return True


def o365_fill_form(browser, email, password) -> bool:

    #locating email field and entering the email
    elem = browser.find_element_by_id(ZEUS_O365_EMAIL_ID)
    elem.send_keys(email)
    elem.send_keys(Keys.ENTER)

    time.sleep(8)

    #locating password field and entering the password
    elem2 = browser.find_element_by_id(ZEUS_O365_PASSWORD_ID)
    elem2.send_keys(password)

    time.sleep(1)

    elem2.submit()

    time.sleep(4)

    return o365_stay_signed(browser)


def fetch_zeus_auth_token(email, password, headless=True) -> str:
    options = Options()
    options.headless = headless

    browser = webdriver.Firefox(options=options)
    # get the local storage
    storage = LocalStorage(browser)

    browser.get(ZEUS_URL)

    login_o365(browser, email, password)

    url = browser.current_url
    print(f"Current url: {url}")
    if "zeus" in url:
        print("Logged successfully")
    else:
        print("Failed to login... Abort.")
        return None

    auth_data = storage.get(ZEUS_AUTH_KEY)
    browser.close()
    if not auth_data:
        print("Cannot dump local storage... Abort.")
        return None

    return json.loads(auth_data).get("token")

