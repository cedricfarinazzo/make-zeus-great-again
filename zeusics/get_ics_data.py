import requests
from icalendar import Calendar

from .data import *

def fix_encoding(name):
    return name.replace(b'\xc3\x83\xc2\xa8', b'\xc3\xa8').replace(b'\xc3\x83\xc2\xa9', b'\xc3\xa9')


def update_ics(ics_data):
    ical = Calendar().from_ical(ics_data)

    for event in ical.walk():
        if event.name == "VEVENT" and event.get("URL", None):
            event.add("LOCATION", event.get("URL", None))

    return ical.to_ical().decode()


def get_ics_data(auth_token, group_id) -> str:
    ics_url = f"{ZEUS_URL}/api/group/{group_id}/ics"

    print(f"Downloading ics for group {group_id}...")

    headers_dict = {"Authorization": f"Bearer {auth_token}"}
    response = requests.get(ics_url, headers=headers_dict)
    data = response.text
    if response.status_code == 200 and data:
        data = update_ics(fix_encoding(data.encode()).decode())

    return data if response.status_code == 200 else None
