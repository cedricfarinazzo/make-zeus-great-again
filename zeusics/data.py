ZEUS_URL = "https://zeus.ionis-it.com"
ZEUS_AUTH_KEY = "ZEUS-AUTH"
ZEUS_AUTH_CACHE_FILE = "/tmp/zeus_auth_data.json"

ZEUS_LOGIN_O365_BTN_CLASS = "btn-office"
ZEUS_O365_EMAIL_ID = "i0116"
ZEUS_O365_PASSWORD_ID = "passwordInput"
ZEUS_O365_STAY_YES = "idSIButton9"
ZEUS_O365_STAY_NO = "idBtn_Back"
