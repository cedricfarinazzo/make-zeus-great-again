import json
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

SCOPES = ['https://www.googleapis.com/auth/calendar']

CREDENTIALS_FILE = 'credentials.json'

def authorize_and_gen_refresh_token(credentials_path, refresh_token_path):
    flow = InstalledAppFlow.from_client_secrets_file(
            credentials_path, SCOPES)
    creds = flow.run_local_server(port=0)
    if creds or creds.valid:
        with open(refresh_token_path, "w+") as f:
            f.write(creds.refresh_token)
            return True
    return False

def get_calendar_service(credentials_path, refresh_token_path):
    with open(credentials_path, "r") as openid:
        openid_data = json.load(openid)
        with open(refresh_token_path, "r") as rf:
            creds = Credentials(token=None,
                    refresh_token=rf.read(), scopes=SCOPES,
                    client_id = openid_data["installed"]["client_id"],
                    client_secret = openid_data["installed"]["client_secret"],
                    token_uri = openid_data["installed"]["token_uri"],
                    )
            creds.refresh(Request())
    service = build('calendar', 'v3', credentials=creds)
    return service
