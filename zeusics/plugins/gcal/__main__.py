import argparse
from . import authorize_and_gen_refresh_token

def main():
    parser = argparse.ArgumentParser(description='Authorize and generate gauth refresh token for the zeusics gcal plugin.')
    parser.add_argument("credentials_path", type=str, help="ID client OAuth credentials file path (json file)")
    parser.add_argument("refresh_token_file_path", type=str, help="Where to write the gauth refresh token")
    args = parser.parse_args()

    authorize_and_gen_refresh_token(args.credentials_path, args.refresh_token_file_path)

if __name__ == '__main__':
    main()
