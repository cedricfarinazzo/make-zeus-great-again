import datetime
from icalendar import Calendar, Event, vText
from googleapiclient.http import BatchHttpRequest

from .gcal_service import get_calendar_service

def get_calendar_id(service, name):
    calendars_result = service.calendarList().list().execute()
    calendars = calendars_result.get('items', [])

    if not calendars:
        return None

    for calendar in calendars:
        if calendar['summary'] == name:
            return calendar['id']
    return None


def ics_to_events(ics_data, blacklist = []):
    events = []
    ical = Calendar().from_ical(ics_data)

    ical_config = dict(ical.sorted_items())
    for i, comp in enumerate(ical.walk()):
        if comp.name == 'VEVENT':
            event = {}
            for name, prop in comp.property_items():

                if name in ['SUMMARY', 'LOCATION']:
                    event[name.lower()] = prop.to_ical().decode('utf-8')

                elif name == 'DTSTART':
                    event['start'] = {
                        'dateTime': prop.dt.isoformat(),
                        'timeZone': str(prop.dt.tzinfo)
                    }

                elif name == 'DTEND':
                    event['end'] = {
                        'dateTime': prop.dt.isoformat(),
                        'timeZone': str(prop.dt.tzinfo)
                    }

                elif name == 'SEQUENCE':
                    event[name.lower()] = prop

                elif name == 'TRANSP':
                    event['transparency'] = prop.lower()

                elif name == 'CLASS':
                    event['visibility'] = prop.lower()

                elif name == 'ORGANIZER':
                    event['organizer'] = {
                        'displayName': prop.params.get('CN') or '',
                        'email': re.match('mailto:(.*)', prop).group(1) or ''
                    }

                elif name == 'DESCRIPTION':
                    desc = prop.to_ical().decode('utf-8')
                    desc = desc.replace(u'\xa0', u' ')
                    if name.lower() in event:
                        event[name.lower()] = desc + '\r\n' + event[name.lower()]
                    else:
                        event[name.lower()] = desc

                elif name == 'X-ALT-DESC' and 'description' not in event:
                    soup = BeautifulSoup(prop, 'lxml')
                    desc = soup.body.text.replace(u'\xa0', u' ')
                    if 'description' in event:
                        event['description'] += '\r\n' + desc
                    else:
                        event['description'] = desc

                elif name == 'ATTENDEE':
                    if 'attendees' not in event:
                        event['attendees'] = []
                    RSVP = prop.params.get('RSVP') or ''
                    RSVP = 'RSVP={}'.format('TRUE:{}'.format(prop) if RSVP == 'TRUE' else RSVP)
                    ROLE = prop.params.get('ROLE') or ''
                    event['attendees'].append({
                        'displayName': prop.params.get('CN') or '',
                        'email': re.match('mailto:(.*)', prop).group(1) or '',
                        'comment': ROLE
                    })

                # VALARM: only remind by UI popup
                elif name == 'ACTION':
                    event['reminders'] = {'useDefault': True}

                else:
                    # print(name)
                    pass

            #print(event)
            if not event.get('summary', 'NO_SUMMARY') in blacklist:
                events.append(event)
            else:
                print(event['summary'], 'is blacklisted')
    return events


def push_ics_data(service, calendar_id, ics_data, batch, blacklist = []):
    events = ics_to_events(ics_data, blacklist)
    for e in events:
        batch.add(service.events().insert(calendarId=calendar_id, body=e))


def delete_all_events(service, calendar_id, batch):
    now = datetime.datetime.utcnow().isoformat() + 'Z'

    events_result = service.events().list(
            calendarId=calendar_id, timeMin=now,
            maxResults=2500, singleEvents=True,
            orderBy='startTime').execute()
    events = events_result.get('items', [])

    if not events:
        return
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        batch.add(service.events().delete(calendarId=calendar_id, eventId=event['id']))


def update_gcal(calendar_name, ics_data, credentials_path, refresh_token_path, blacklist = []):

    service = get_calendar_service(credentials_path, refresh_token_path)
    calendar_id = get_calendar_id(service, calendar_name)

    batch = service.new_batch_http_request()

    delete_all_events(service, calendar_id, batch)

    push_ics_data(service, calendar_id, ics_data, batch, blacklist)

    batch.execute()
    print(f"Gcal: {calendar_name} calendar updated")

