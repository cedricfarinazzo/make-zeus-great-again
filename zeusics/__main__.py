import sys
import os
import argparse

from .data import *
from . import fetch_zeus_auth_token, get_ics_data, test_zeus_auth

from .plugins import update_gcal

def main():
    parser = argparse.ArgumentParser(description='Download ics from Zeus.')
    parser.add_argument("email", type=str, help="Your Office 365 email")
    parser.add_argument("password", type=str, help="Your Office 365 password")
    parser.add_argument("group_id", type=int, help="Will download the ics from this group")
    parser.add_argument("output_ics", type=str, help="Output file")

    parser.add_argument("--headless-disabled", default=False, action='store_true', help="Disable headless mode")
    parser.add_argument("--auth-token-cache-file", default=ZEUS_AUTH_CACHE_FILE, help="Where to store your authentification token")


    parser.add_argument("--gcal-calendar-name", type=str, help="Google Calendar name (where to push events)")
    parser.add_argument("--gcal-credentials-path", type=str, help="ID client OAuth credentials file path (json file)")
    parser.add_argument("--gcal-refresh-token-file-path", type=str, help="The file containing the gauth refresh token")
    parser.add_argument("--blacklist", type=str, default="blacklist.txt", help="File of courses to blacklist")
    args = parser.parse_args()

    auth_token = None
    auth_token_cache_file = args.auth_token_cache_file
    if os.path.exists(auth_token_cache_file):
        print(f"Loading local storage from {auth_token_cache_file}")
        with open(auth_token_cache_file, "r") as f:
            auth_token = f.read()

    if not test_zeus_auth(auth_token):
        print("Failed... refreshing token...")
        auth_token = fetch_zeus_auth_token(args.email, args.password, headless= not args.headless_disabled)
        if not auth_token:
            print("Failed to retrieve auth token... Abort.")
            sys.exit(1)
        with open(auth_token_cache_file, "w+") as f:
            f.write(auth_token)
            print(f"Local storage cached in {auth_token_cache_file}")

    ics_data = get_ics_data(auth_token, args.group_id)
    if not ics_data:
        print("Failed to download ics ... Abort.")
        sys.exit(1)

    with open(args.output_ics, "w+") as f:
        f.write(ics_data)


    if args.gcal_calendar_name and args.gcal_credentials_path and args.gcal_refresh_token_file_path:
        try:
            with open(args.blacklist, 'r') as f:
                blacklist = [ i.strip() for i in f.readlines()]
        except FileNotFoundError:
            blacklist = []
        update_gcal(args.gcal_calendar_name, ics_data, args.gcal_credentials_path, args.gcal_refresh_token_file_path, blacklist)

if __name__ == '__main__':
    main()
