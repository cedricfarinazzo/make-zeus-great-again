#!/bin/sh

python3 -m zeusics \
    --auth-token-cache-file /cache/zeus_token.cache \
    --gcal-calendar-name "$ZEUS_GCAL_NAME" \
    --gcal-credentials-path /etc/gcal/creds/gcal_credentials.json \
    --gcal-refresh-token-file-path /etc/gcal/creds/gcal_refresh_token.txt \
    --blacklist /etc/gcal/creds/blacklist.txt \
    "$ZEUS_EMAIL" "$ZEUS_PASSWORD" "$ZEUS_GROUP_ID" /cache/current.ics
