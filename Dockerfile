FROM ubuntu

ENV IN_DOCKER=1
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# install geckodriver and firefox

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends firefox python3 python3-pip wget && \
    rm -rf /var/lib/apt/lists/*

RUN GECKODRIVER_VERSION=v0.30.0 && \
    wget https://github.com/mozilla/geckodriver/releases/download/$GECKODRIVER_VERSION/geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz && \
    tar -zxf geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz -C /usr/local/bin && \
    chmod +x /usr/local/bin/geckodriver && \
    rm geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz

COPY requirements.txt .

# Install python package
RUN python3 -m pip install --no-cache-dir -r requirements.txt

# Copy the application
COPY . .

COPY k8s/k8s_entrypoint.sh /bin/k8s_entrypoint.sh

ENTRYPOINT [ "python3", "-m", "zeusics" ]
