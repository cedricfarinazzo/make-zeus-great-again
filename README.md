# Make Zeus Great Again


``Make Zeus Great Again`` was created to allow EPITA students to automatically
download ics files from [Zeus](https://zeus.3ie.fr) and updates directly their calendar.

## Description
This python package uses your Office 365 credentials to connect to Zeus and retrieve the ics of the group of your choice.

It uses [Selenium](https://selenium-python.readthedocs.io/) and Firefox to authenticate you in O365 in headless mode.
Then it retrieves the authentification token from the local storage of Zeus.
With that, it uses [Requests](https://docs.python-requests.org/en/latest/) to download the ics.

Cache is used to prevent login every time in Office 365.

With the gcal plugin, your calendar will always be sync with Zeus!

## Advantages

- No public ics, everything is private
- Serverless
- Cache handling
- Sync can be triggerred when you want
- Kubernetes compatible


## Installation

### Pip
- Install Firefox and [Geckodriver](https://github.com/mozilla/geckodriver)
- ``pip install -r requirements.txt``

### Docker
```
docker pull registry.gitlab.com/cedricfarinazzo/make-zeus-great-again/zeusics:v1.2
```

## Usage
```
sh $ python -m zeusics --help
usage: __main__.py [-h] [--headless-disabled] [--auth-token-cache-file AUTH_TOKEN_CACHE_FILE] [--gcal-calendar-name GCAL_CALENDAR_NAME] [--gcal-credentials-path GCAL_CREDENTIALS_PATH]
                   [--gcal-refresh-token-file-path GCAL_REFRESH_TOKEN_FILE_PATH]
                   email password group_id output_ics

Download ics from Zeus.

positional arguments:
  email                 Your Office 365 email
  password              Your Office 365 password
  group_id              Will download ths ics from this group
  output_ics            Output file

optional arguments:
  -h, --help            show this help message and exit
  --headless-disabled   Disable headless mode
  --auth-token-cache-file AUTH_TOKEN_CACHE_FILE
                        Where to store your authentification token
  --gcal-calendar-name GCAL_CALENDAR_NAME
                        Google Calendar name (where to push events)
  --gcal-credentials-path GCAL_CREDENTIALS_PATH
                        ID client OAuth credentials file path (json file)
  --gcal-refresh-token-file-path GCAL_REFRESH_TOKEN_FILE_PATH
                        The file containing the gauth refresh token
  --blacklist BLACKLIST
                        File of courses to blacklist
```

I recommend that you do not use your credentials directly as an argument.

You can use your password manager's CLI for this.

If you can't, turn off your shell history with ``set +o history``

In order to prevent some courses from appearing in google calendar, you can use a blacklist file.
A blacklist file contains a course name per line.
For example, blacklisting minors is as follow:
```
Ethique du numérique
Information quantique
Management interculturel
MOOC : Devenir Créatif et Innovant
Mooc : Economie Collaborative
Comment créer son entreprise
[Mineure de Finance] Produits Financiers
[Mineure de Finance] Mathématiques & Finances de marché
[Mineure de Finance] Risques bancaires
Marketing Digital
Droit du travail
```

The default blacklist file path is `blacklist.txt`

### Docker
```
docker run -it --rm registry.gitlab.com/cedricfarinazzo/make-zeus-great-again/zeusics:v1.2 --help
```

## Plugins

### Google Calendar
This plugin updates your Google Calendar using Google Cloud API.

#### Installation

To use this plugin, you need to create a service that will be allowed to update your calendar.

This following is a simple guide. Or follow this [tutorial](https://karenapp.io/articles/how-to-automate-google-calendar-with-python-using-the-calendar-api/).

You have to create a new project in the [Google Cloud Platform Dashboard](https://console.cloud.google.com/apis/dashboard).
Then, you can go to the ``APIs & Services`` section. Click on ``ENABLE APIS AND SERVICES`` on the top.
Search the ``Google Calendar API`` and enable it.

Then go back to the ``APIs & Services`` section, and click on ``OAuth consent screen`` in the sidebar.
Click on ``External``, then on ``CREATE``. Fill all required fields.
In the ``scopes`` list, add ``.../auth/calendar.events`` for the Google Calendar API.
Add your email in the test users form.
Your OAuth consent screen is configured.

Now, click on ``Credentials`` in the sidebar, and create OAuth client ID credentials.
The application type is ``Desktop app``.
Download the json file. It will be used by the gcal plugin.

In Google Calendar, create a new calendar.

Now you have to authorize the service and generate the refresh token.
```
sh $ python -m zeusics.plugins.gcal --help
usage: __main__.py [-h] credentials_path refresh_token_file_path

Authorize and generate gauth refresh token for the zeusics gcal plugin.

positional arguments:
  credentials_path      ID client OAuth credentials file path (json file)
  refresh_token_file_path
                        Where to write the gauth refresh token

optional arguments:
  -h, --help            show this help message and exit
```
You can use this script. It will open a tab in your browser and generate the refresh token file.

Everything is configured!

### Usage

Specify the arguments:
- ``--gcal-calendar-name``
- ``--gcal-credentials-path``
- ``--gcal-refresh-token-file-path``


## Contributing
If you want to improve this tool, I accept gladly merge requests.

## License
This project is free to use. You are allowed to modify it and to use it for personal use only.
Use it at your own risk, I am not responsible for your use of it.
